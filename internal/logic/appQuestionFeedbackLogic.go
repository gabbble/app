package logic

import (
	"context"
	"database/sql"
	"gitee.com/gabbble/app/model/mysql"
	"gitee.com/gabbble/comm_pkg/common/constant"
	"gitee.com/gabbble/comm_pkg/utils"
	"time"

	"gitee.com/gabbble/app/apppb"
	"gitee.com/gabbble/app/internal/svc"

	"github.com/zeromicro/go-zero/core/logx"
)

type AppQuestionFeedbackLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewAppQuestionFeedbackLogic(ctx context.Context, svcCtx *svc.ServiceContext) *AppQuestionFeedbackLogic {
	return &AppQuestionFeedbackLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *AppQuestionFeedbackLogic) AppQuestionFeedback(in *apppb.AppQuestionFeedbackReq) (*apppb.AppQuestionFeedbackRsp, error) {
	fun := utils.GetSelfFuncName() + " --"
	_, err := l.svcCtx.ModelInstQuestionFeedback.Insert(l.ctx, &mysql.AppQuestionFeedback{
		Id:       0,
		Uid:      in.Uid,
		Uuid:     in.Uuid,
		Platform: constant.PlatformIDToName(int(in.Platform)),
		Email:    in.Email,
		Content: sql.NullString{
			String: in.Content,
			Valid:  true,
		},
		Ctime: time.Now().Unix(),
	})
	if err != nil {
		logx.WithContext(l.ctx).Infof("%s Insert Fail: %v, req: %+v", fun, err, in)
		return &apppb.AppQuestionFeedbackRsp{
			ErrInfo: &apppb.ErrorInfo{
				ErrCode: -1,
				ErrMsg:  err.Error(),
			},
		}, nil
	}
	return &apppb.AppQuestionFeedbackRsp{
		ErrInfo: &apppb.ErrorInfo{
			ErrCode: 0,
			ErrMsg:  "success",
		},
	}, nil
}
