package logic

import (
	"context"
	"database/sql"
	"gitee.com/gabbble/app/model/mysql"
	"gitee.com/gabbble/comm_pkg/utils"
	"time"

	"gitee.com/gabbble/app/apppb"
	"gitee.com/gabbble/app/internal/svc"
	"gitee.com/gabbble/comm_pkg/common/constant"

	"github.com/zeromicro/go-zero/core/logx"
)

type AppLogReportLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewAppLogReportLogic(ctx context.Context, svcCtx *svc.ServiceContext) *AppLogReportLogic {
	return &AppLogReportLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *AppLogReportLogic) AppLogReport(in *apppb.AppLogReportReq) (*apppb.AppLogReportRsp, error) {
	fun := utils.GetSelfFuncName() + " --"
	_, err := l.svcCtx.ModelInstAppLogReportRecord.Insert(l.ctx, &mysql.AppLogReportRecord{
		Id:         0,
		Uid:        in.Uid,
		LogType:    in.LogType,
		Uuid:       in.Uuid,
		Platform:   constant.PlatformIDToName(int(in.Platform)),
		AppVersion: in.AppVersion,
		Content: sql.NullString{
			String: in.Content,
			Valid:  true,
		},
		Ctime: time.Now().Unix(),
	})
	if err != nil {
		logx.WithContext(l.ctx).Infof("%s Insert Fail: %v, req: %+v", fun, err, in)
		return &apppb.AppLogReportRsp{
			ErrInfo: &apppb.ErrorInfo{
				ErrCode: -1,
				ErrMsg:  err.Error(),
			},
		}, nil
	}
	return &apppb.AppLogReportRsp{
		ErrInfo: &apppb.ErrorInfo{
			ErrCode: 0,
			ErrMsg:  "success",
		},
	}, nil
}
