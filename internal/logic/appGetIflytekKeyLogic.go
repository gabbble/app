package logic

import (
	"context"
	"gitee.com/gabbble/comm_pkg/utils"

	"gitee.com/gabbble/app/apppb"
	"gitee.com/gabbble/app/internal/svc"

	"github.com/zeromicro/go-zero/core/logx"
)

type AppGetIflytekKeyLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewAppGetIflytekKeyLogic(ctx context.Context, svcCtx *svc.ServiceContext) *AppGetIflytekKeyLogic {
	return &AppGetIflytekKeyLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *AppGetIflytekKeyLogic) AppGetIflytekKey(in *apppb.AppGetIflytekKeyReq) (*apppb.AppGetIflytekKeyRsp, error) {
	fun := utils.GetSelfFuncName() + " --"
	info, err := l.svcCtx.ModelInstAppLflytekKeyInfo.FindOneLatest(l.ctx)
	if err != nil {
		logx.WithContext(l.ctx).Infof("%s Find Fail: %v, req: %+v", fun, err, in)
		return &apppb.AppGetIflytekKeyRsp{
			ErrInfo: &apppb.ErrorInfo{
				ErrCode: -1,
				ErrMsg:  err.Error(),
			},
		}, nil
	}
	return &apppb.AppGetIflytekKeyRsp{
		Data: &apppb.AppGetIflytekKeyData{
			AppID:     info.AppID,
			ApiSecret: info.ApiSecret,
			ApiKey:    info.ApiKey,
		},
	}, nil
}
