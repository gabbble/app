package svc

import (
	"gitee.com/gabbble/app/internal/config"
	"gitee.com/gabbble/app/model/mysql"
	"github.com/zeromicro/go-zero/core/stores/sqlx"
)

type ServiceContext struct {
	Config                      config.Config
	ModelInstAppLflytekKeyInfo  mysql.AppLflytekKeyInfoModel
	ModelInstAppLogReportRecord mysql.AppLogReportRecordModel
	ModelInstQuestionFeedback   mysql.AppQuestionFeedbackModel
}

func NewServiceContext(c config.Config) *ServiceContext {
	return &ServiceContext{
		Config:                      c,
		ModelInstAppLflytekKeyInfo:  mysql.NewAppLflytekKeyInfoModel(sqlx.NewMysql(c.DataSource)),
		ModelInstAppLogReportRecord: mysql.NewAppLogReportRecordModel(sqlx.NewMysql(c.DataSource)),
		ModelInstQuestionFeedback:   mysql.NewAppQuestionFeedbackModel(sqlx.NewMysql(c.DataSource)),
	}
}
