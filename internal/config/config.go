package config

import (
	"github.com/zeromicro/go-zero/core/logx"
	"github.com/zeromicro/go-zero/zrpc"
)

type Config struct {
	LogConf logx.LogConf
	zrpc.RpcServerConf
	DataSource string
}
