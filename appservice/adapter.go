package appservice

import (
	"errors"
	"github.com/zeromicro/go-zero/core/discov"
	"github.com/zeromicro/go-zero/core/logx"
	"github.com/zeromicro/go-zero/zrpc"
	"time"
)

const service_key_grpc = "base/app.rpc"

var rpcClient zrpc.Client

func init() {
	go func() {
		var err error
		var tryCnt time.Duration
		for {
			conf := zrpc.RpcClientConf{
				Etcd: discov.EtcdConf{
					Hosts: []string{"infra0.etcd.gabbble.com:2379"},
					Key:   service_key_grpc,
				},
			}

			rpcClient, err = zrpc.NewClient(conf)
			if err != nil {
				logx.Errorf("%s new client fail:%v conf:%+v", service_key_grpc, err, conf)
			} else {
				break
			}

			time.Sleep(time.Second*tryCnt + time.Millisecond*100)
			tryCnt++
			if tryCnt > 5 {
				tryCnt = 5
			}
		}
	}()
}

func NewService() (AppService, error) {
	if rpcClient == nil {
		return nil, errors.New("service not found")
	}

	return NewAppService(rpcClient), nil
}
