package mysql

import "github.com/zeromicro/go-zero/core/stores/sqlx"

var _ AppLflytekKeyInfoModel = (*customAppLflytekKeyInfoModel)(nil)

type (
	// AppLflytekKeyInfoModel is an interface to be customized, add more methods here,
	// and implement the added methods in customAppLflytekKeyInfoModel.
	AppLflytekKeyInfoModel interface {
		appLflytekKeyInfoModel
	}

	customAppLflytekKeyInfoModel struct {
		*defaultAppLflytekKeyInfoModel
	}
)

// NewAppLflytekKeyInfoModel returns a model for the database table.
func NewAppLflytekKeyInfoModel(conn sqlx.SqlConn) AppLflytekKeyInfoModel {
	return &customAppLflytekKeyInfoModel{
		defaultAppLflytekKeyInfoModel: newAppLflytekKeyInfoModel(conn),
	}
}
