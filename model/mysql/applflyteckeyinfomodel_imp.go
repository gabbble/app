package mysql

import (
	"context"
	"fmt"
	"github.com/zeromicro/go-zero/core/stores/sqlc"
)

func (m *defaultAppLflytekKeyInfoModel) FindOneLatest(ctx context.Context) (*AppLflytekKeyInfo, error) {
	query := fmt.Sprintf("select %s from %s order by ctime desc limit 1", appLflytekKeyInfoRows, m.table)
	var resp AppLflytekKeyInfo
	err := m.conn.QueryRowCtx(ctx, &resp, query)
	switch err {
	case nil:
		return &resp, nil
	case sqlc.ErrNotFound:
		return nil, ErrNotFound
	default:
		return nil, err
	}
}
