package mysql

import "github.com/zeromicro/go-zero/core/stores/sqlx"

var _ AppQuestionFeedbackModel = (*customAppQuestionFeedbackModel)(nil)

type (
	// AppQuestionFeedbackModel is an interface to be customized, add more methods here,
	// and implement the added methods in customAppQuestionFeedbackModel.
	AppQuestionFeedbackModel interface {
		appQuestionFeedbackModel
	}

	customAppQuestionFeedbackModel struct {
		*defaultAppQuestionFeedbackModel
	}
)

// NewAppQuestionFeedbackModel returns a model for the database table.
func NewAppQuestionFeedbackModel(conn sqlx.SqlConn) AppQuestionFeedbackModel {
	return &customAppQuestionFeedbackModel{
		defaultAppQuestionFeedbackModel: newAppQuestionFeedbackModel(conn),
	}
}
