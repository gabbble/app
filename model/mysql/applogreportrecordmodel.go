package mysql

import "github.com/zeromicro/go-zero/core/stores/sqlx"

var _ AppLogReportRecordModel = (*customAppLogReportRecordModel)(nil)

type (
	// AppLogReportRecordModel is an interface to be customized, add more methods here,
	// and implement the added methods in customAppLogReportRecordModel.
	AppLogReportRecordModel interface {
		appLogReportRecordModel
	}

	customAppLogReportRecordModel struct {
		*defaultAppLogReportRecordModel
	}
)

// NewAppLogReportRecordModel returns a model for the database table.
func NewAppLogReportRecordModel(conn sqlx.SqlConn) AppLogReportRecordModel {
	return &customAppLogReportRecordModel{
		defaultAppLogReportRecordModel: newAppLogReportRecordModel(conn),
	}
}
