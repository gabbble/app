CREATE TABLE `app_lflytek_key_info`
(
    `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `appID` varchar(255) NOT NULL DEFAULT '' COMMENT '日志类型',
    `apiSecret` varchar(255) NOT NULL DEFAULT '' COMMENT 'uuid',
    `apiKey` varchar(255) NOT NULL DEFAULT '' COMMENT '系统',
    `ctime` bigint(20) NOT NULL DEFAULT 0 COMMENT '修改时间',
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT '科大讯飞key信息';
