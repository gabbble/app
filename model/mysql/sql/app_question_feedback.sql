CREATE TABLE `app_question_feedback`(
    `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `uid` bigint(20) NOT NULL DEFAULT 0 COMMENT 'user id',
    `uuid` varchar(255) NOT NULL DEFAULT '' COMMENT 'uuid',
    `platform` varchar(255) NOT NULL DEFAULT '' COMMENT '系统',
    `email` varchar(255) NOT NULL DEFAULT '' COMMENT '邮箱',
    `content` TEXT COMMENT '反馈内容',
    `ctime` bigint(20) NOT NULL DEFAULT 0 COMMENT '修改时间',
    PRIMARY KEY (`id`),
    KEY idx_u(`uid`),
    KEY idx_c(`ctime`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT '客户端问题反馈记录';